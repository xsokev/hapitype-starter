import * as Dotenv from 'dotenv';

import logger from '../utilities/logger';
import chalk from 'chalk';
import process from 'process';
import crypto from 'crypto';

//This should be before anything else
Dotenv.config();

if (process.env.NODE_ENV === 'development') {
    console.log(chalk.bgBlack(chalk.cyan(`
==========================================
Server is running in development mode,
therefore cannot not use compiled JS files
If you want to use compiled JS files from
"dist" then Set NODE_ENV to a value other
than "development"
==========================================
    `)));
}


// Create deployment version
let tmp_version: string = crypto.randomBytes(20).toString('hex');
if (process.env.STATIC_FILES_VERSION) {
    tmp_version = process.env.STATIC_FILES_VERSION;
}

if (!process.env.HOST) {
    throw new Error(`Please specify server HOST environment variable`);
} else {
    logger.info(`Using HOST from Environment variable`);
}

if (!process.env.PORT) {
    throw new Error(`Please specify server PORT environment variable`);
} else {
    logger.info(`Using PORT from Environment variable`);
}

export const NODE_ENV:any = process.env.NODE_ENV;
export const PORT:any = process.env.PORT;
export const HOST:any = process.env.HOST;

// Deployment version
export const DEPLOYMENT_VERSION: string = tmp_version;

//Security
export const USE_AUTH = process.env.USE_AUTH;
export const DEFAULT_AUTH = process.env.DEFAULT_AUTH;
export const USE_ROLES = process.env.USE_ROLES;
export const USE_IP_LIMIT = process.env.USE_IP_LIMIT;
export const USE_APIKEY = process.env.USE_APIKEY;
export const API_KEY = process.env.API_KEY;

//All others in global
export const GLOBAL:any = process.env;