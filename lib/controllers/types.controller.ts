import { Type } from "@database/entities/type.entity";
import { ServerRoute } from "@hapi/hapi";
import { ControllerFactory } from "./factory";
import { getConnection, Repository } from "typeorm";
import { CreateTypeSchema } from "@database/dto/create.type.dto";
import { UpdateTypeSchema } from "@database/dto/update.type.dto";

export const typeController = (): Array<ServerRoute> => {
    const repository:Repository<Type> = getConnection().getRepository(Type);
    return ControllerFactory<Type>({
        repository,
        name: 'Type',
        createSchema: CreateTypeSchema,
        updateSchema: UpdateTypeSchema,
        authentication: {
            create: 'firebase',
            update: 'firebase',
            remove: 'firebase',
        },
        authorizations: {
            find: ['superadmin'],
            update: ['superadmin']
        }
    })
}
