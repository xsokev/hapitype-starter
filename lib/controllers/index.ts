import {typeController} from '@controllers/types.controller';
import { ServerRoute } from '@hapi/hapi';

const initControllerRoutes = () => {
    let routes: ServerRoute[] = [];

    routes = routes.concat(typeController());

    return routes;
};
export default initControllerRoutes;