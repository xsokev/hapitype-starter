import * as Boom from "@hapi/boom";
import { ResponseToolkit, ServerRoute, Request, RouteOptionsAccess } from "@hapi/hapi";
import Joi from "joi";
import { DeepPartial, DeleteResult, Repository, UpdateResult } from "typeorm";

export interface ControllerFactoryOptions<T> {
    repository: Repository<T>
    name: string
    routes?: Array<ServerRoute>
    payloadAllow?: string | Array<string>
    createSchema?: Joi.ObjectSchema
    updateSchema?: Joi.ObjectSchema
    authentication?: {
        all?: string | false | RouteOptionsAccess,
        find?: string | false | RouteOptionsAccess,
        get?: string | false | RouteOptionsAccess,
        create?: string | false | RouteOptionsAccess,
        update?: string | false | RouteOptionsAccess,
        patch?: string | false | RouteOptionsAccess,
        remove?: string | false | RouteOptionsAccess,
    },
    authorizations?: {
        all?: false | Array<string>,
        find?: false | Array<string>,
        get?: false | Array<string>,
        create?: false | Array<string>,
        update?: false | Array<string>,
        patch?: false | Array<string>,
        remove?: false | Array<string>,
    },
}

export function ControllerFactory<T>({name, repository, routes, payloadAllow='application/json', createSchema, updateSchema, authentication, authorizations}: ControllerFactoryOptions<T>): Array<any> {
    const path: string = name.toLowerCase();
    const _find = async (id: number) => {
        const item = await repository.findOne(id);
        if(item){
            return item;
        }
        throw Boom.notFound(`${name} not found with id!`)
    }

    let findOpts = {}, getOpts = {}, createOpts = {}, updateOpts = {}, patchOpts = {}, removeOpts = {};
    let findPlugs, getPlugs, createPlugs, updatePlugs, patchPlugs, removePlugs;
    if(authorizations){
        if(authorizations.all === false){
            findPlugs = {'hapiAuthorization': false};
            getPlugs = {'hapiAuthorization': false};
            createPlugs = {'hapiAuthorization': false};
            updatePlugs = {'hapiAuthorization': false};
            patchPlugs = {'hapiAuthorization': false};
            removePlugs = {'hapiAuthorization': false};
        } else if(authorizations.all){
            findPlugs = {'hapiAuthorization': {roles: authorizations.all}}
            getPlugs = {'hapiAuthorization': {roles: authorizations.all}}
            createPlugs = {'hapiAuthorization': {roles: authorizations.all}}
            updatePlugs = {'hapiAuthorization': {roles: authorizations.all}}
            patchPlugs = {'hapiAuthorization': {roles: authorizations.all}}
            removePlugs = {'hapiAuthorization': {roles: authorizations.all}}
        }
        if(authorizations.find === false){
            findPlugs = {'hapiAuthorization': false};
        } else if(authorizations.find) {
            findPlugs = {'hapiAuthorization': {roles: authorizations.find}}
        }
        if(authorizations.get === false){
            getPlugs = {'hapiAuthorization': false};
        } else if(authorizations.get) {
            getPlugs = {'hapiAuthorization': {roles: authorizations.get}}
        }
        if(authorizations.create === false){
            createPlugs = {'hapiAuthorization': false};
        } else if(authorizations.create) {
            createPlugs = {'hapiAuthorization': {roles: authorizations.create}}
        }
        if(authorizations.update === false){
            updatePlugs = {'hapiAuthorization': false};
        } else if(authorizations.update) {
            updatePlugs = {'hapiAuthorization': {roles: authorizations.update}}
        }
        if(authorizations.patch === false){
            patchPlugs = {'hapiAuthorization': false};
        } else if(authorizations.patch) {
            patchPlugs = {'hapiAuthorization': {roles: authorizations.patch}}
        }
        if(authorizations.remove === false){
            removePlugs = {'hapiAuthorization': false};
        } else if(authorizations.remove) {
            removePlugs = {'hapiAuthorization': {roles: authorizations.remove}}
        }
    }
    if(authentication){
        if(authentication.all === false){
            findOpts = {auth: false};
            getOpts = {auth: false};
            createOpts = {auth: false};
            updateOpts = {auth: false};
            patchOpts = {auth: false};
            removeOpts = {auth: false};
        } else if(authentication.all){
            findOpts = {auth: authentication.all, plugins: findPlugs};
            getOpts = {auth: authentication.all, plugins: getPlugs};
            createOpts = {auth: authentication.all, plugins: createPlugs};
            updateOpts = {auth: authentication.all, plugins: updatePlugs};
            patchOpts = {auth: authentication.all, plugins: patchPlugs};
            removeOpts = {auth: authentication.all, plugins: removePlugs};
        }
        if(authentication.find === false){
            findOpts = {auth: false};
        } else if(authentication.find) {
            findOpts = {auth: {roles: authentication.find}}
        }
        if(authentication.get === false){
            getOpts = {auth: false};
        } else if(authentication.get) {
            getOpts = {auth: authentication.get, plugins: getPlugs};
        }
        if(authentication.create === false){
            createOpts = {auth: false};
        } else if(authentication.create) {
            createOpts = {auth: authentication.create, plugins: createPlugs};
        }
        if(authentication.update === false){
            updateOpts = {auth: false};
        } else if(authentication.update) {
            updateOpts = {auth: authentication.update, plugins: updatePlugs};
        }
        if(authentication.patch === false){
            patchOpts = {auth: false};
        } else if(authentication.patch) {
            patchOpts = {auth: authentication.patch, plugins: patchPlugs};
        }
        if(authentication.remove === false){
            removeOpts = {auth: false};
        } else if(authentication.remove) {
            removeOpts = {auth: authentication.remove, plugins: removePlugs};
        }
    }
    console.log(Object.assign(updateOpts, {
        validate: {
            params: Joi.object({
                id: Joi.number().min(1).required()
            })
        }
    }));
    const baseRoutes = [
        {
            method: 'GET',
            path: `/${path}`,
            handler: (req: Request, h: ResponseToolkit, err?: Error): Promise<Array<T>> => {
                return repository.find();
            },
            options: findOpts
        },
        {
            method: 'GET',
            path: `/${path}/{id}`,
            handler: async ({params: {id}}: Request, h: ResponseToolkit, err?: Error): Promise<T> => {
                return _find(id);
            },
            options: Object.assign(getOpts, {
                validate: {
                    params: Joi.object({
                        id: Joi.number().min(1).required()
                    })
                }
            }),
        },
        {
            method: 'POST',
            path: `/${path}`,
            handler: async ({payload}: Request, h: ResponseToolkit, err?: Error): Promise<T> => {
                const item = await repository.create(payload as DeepPartial<T>);
                return await repository.save<DeepPartial<T>>(item);
            },
            options: Object.assign(createOpts, {
                payload: {
                    allow: payloadAllow
                },
                validate: {
                    payload: createSchema?.options({stripUnknown: true})
                }
            }),
        },
        {
            method: 'PUT',
            path: `/${path}/{id}`,
            handler: async ({params: {id}, payload}: Request, h: ResponseToolkit, err?: Error): Promise<UpdateResult> => {
                const item: any = await _find(id);
                for (const key in (payload as DeepPartial<T>)) {
                    item[key] = (payload as DeepPartial<T>)[key];
                }
                return await repository.update(id, item);
            },
            options: Object.assign(updateOpts, {
                payload: {
                    allow: payloadAllow
                },
                validate: {
                    params: Joi.object({
                        id: Joi.number().min(1).required()
                    }),
                    payload: updateSchema?.options({stripUnknown: true})
                }
            }),
        },
        {
            method: 'PATCH',
            path: `/${path}/{id}`,
            handler: async ({params: {id}, payload}: Request, h: ResponseToolkit, err?: Error): Promise<UpdateResult> => {
                const item: any = await _find(id);
                for (const key in (payload as DeepPartial<T>)) {
                    item[key] = (payload as DeepPartial<T>)[key];
                }
                return await repository.update(id, item);
            },
            options: Object.assign(patchOpts, {
                payload: {
                    allow: payloadAllow
                },
                validate: {
                    params: Joi.object({
                        id: Joi.number().min(1).required()
                    }),
                    payload: updateSchema?.options({stripUnknown: true})
                }
            }),
        },
        {
            method: 'DELETE',
            path: `/${path}/{id}`,
            handler: async ({params: {id}}: Request, h: ResponseToolkit, err?: Error): Promise<DeleteResult> => {
                const item: any = await _find(id);
                return await repository.delete(id);
            },
            options: Object.assign(removeOpts, {
                validate: {
                    params: Joi.object({
                        id: Joi.number().min(1).required()
                    }),
                }
            }),
        }
    ];
    return [
        ...baseRoutes,
        ...(routes ? routes : []),
    ];
}
