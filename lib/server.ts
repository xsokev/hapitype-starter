import * as Hapi from '@hapi/hapi';
import Glue from '@hapi/glue';
import * as Boom from '@hapi/boom';
import chalk from 'chalk';
import logger from '@logger';
import * as env from '@configs/env';
import {forEach} from 'lodash'

import DBConnections from './database/';
import * as routes from '@routes/index';
import initControllerRoutes from '@controllers/index';

const _appPlugins = [];
if(env.NODE_ENV !== 'production'){
    try {
        _appPlugins.push({ plugin: require('blipp') });
    } catch(e){
        logger.error('blipp is not installed. Install using: npm i -S blipp');
    }
}
if(env.USE_AUTH){
    try {
        _appPlugins.push({ 
            plugin: require('./plugins/firebase_auth'), 
            options: { validate: async (authKey:string) => {
                return {
                    credentials: {
                        user: {id: 1, firstName: "Kevin", lastName: "Armstrong", role: "admin"}
                    },
                    isValid: true,
                }
            } } 
        });
    } catch(e){
        logger.error('firebase-auth is not installed.');
    }
}
if(env.USE_AUTH && env.USE_ROLES){
    //TODO: possibly use function to retrieve roles from database
    try {
        _appPlugins.push({
            plugin: require('hapi-authorization'),
            options: {
                roles: ['superadmin', 'administrator', 'manager', 'subscriber', 'employee', 'watcher']
            }
        });
    } catch(e){
        logger.error('hapi-authorization is not installed. Install using: npm i -S hapi-authorization');
    }
}
if(env.USE_IP_LIMIT){
    try {
        _appPlugins.push({ plugin: require('hapi-auth-ip-whitelist') });
    } catch(e){
        logger.error('hapi-auth-ip-whitelist is not installed. Install using: npm i -S hapi-auth-ip-whitelist');
    }
}
if(env.USE_APIKEY){
    try {
        _appPlugins.push({ plugin: require('./plugins/api_ext'), options: { apiKey: env.API_KEY } });
    } catch(e){
        logger.error('api-key plugin is not installed!');
    }
}
const manifest:Glue.Manifest = {
    server: {
        host: env.HOST,
        port: env.PORT,
        routes: {
            validate: {
                failAction: async (request:any, h:any, err:any):Promise<void> => {
                    if (env.NODE_ENV === 'production') {
                        // In prod, log a limited error message and throw the default Bad Request error.
                        logger.error('ValidationError:', err.message);
                        throw Boom.badRequest(`Invalid request payload input`);
                    } else {
                        logger.error(err);
                        throw err;
                    }
                }
            }
        },
        state: {
            // If your cookie format is not RFC 6265, set this param to false.
            strictHeader: false
        },
    },
    register: { plugins: _appPlugins }
};

export const getServer: () => Promise<Hapi.Server> = async (): Promise<Hapi.Server> => {
    logger.info(`Server starting: ${new Date().toISOString()}`);

    await DBConnections.init();

    const server = await Glue.compose(manifest, {relativeTo: __dirname });

    let serverRoutes:any = [];

    server.state('token', {
        ttl: null,
        isSecure: (!(env.NODE_ENV === 'development')),
        isHttpOnly: true,
        clearInvalid: true,
        strictHeader: true
    });

    let ROUTE_PREFIX:string = "";
    if (env.GLOBAL.ROUTE_PREFIX) {
        ROUTE_PREFIX = env.GLOBAL.ROUTE_PREFIX;
    }

    forEach(routes, (value:any, key:any) => {
        serverRoutes.push(...value.map((route:any) => {
            route.path = ROUTE_PREFIX + route.path;
            return route;
        }));
    });
    forEach(initControllerRoutes(), (route:any) => {
        route.path = ROUTE_PREFIX + route.path;
        serverRoutes.push(route);
    });
    server.route(serverRoutes);
    return server;
};

export const startServer:() => Promise<void> = async (): Promise<void> => {
    try {
        const server: Hapi.Server = await getServer();
        server.start();

        console.info(chalk.bgBlack(chalk.cyan(`Server running at: ${server.info.uri}`)));
        
        logger.info(`Server started at: ${new Date().toISOString()}`);
    } catch (e) {
        logger.error(e);
    }
};

if (require.main === module) {
    startServer();
} else {
    logger.info('Server setup for testing.');
}