'use strict';

import { Request, ResponseToolkit, Server } from "@hapi/hapi";

const Boom = require('@hapi/boom');
const Hoek = require('@hapi/hoek');

const internals = {
    implementation: function (server: Server, options: any) {
        Hoek.assert(options, 'Missing Firebase ID auth strategy options');
        Hoek.assert(typeof options.validate === 'function', 'options.validate must be a valid function in firebase scheme');

        server.auth.scheme('firebase-auth', (server) => {
            return {
                authenticate: async function (request: Request, h: ResponseToolkit) {
                    const authorization = request.raw.req.headers.authorization;
                    if (!authorization) {
                        throw Boom.unauthorized('Authorization missing!', 'Firebase ID', options.unauthorizedAttributes);
                    }
                    const {isValid, credentials} = await options.validate(authorization);
                    
                    if (!isValid) {
                        return h.unauthenticated(Boom.unauthorized('Invalid Firebase ID', 'Firebase ID', options.unauthorizedAttributes));
                    }
                    if (!credentials || typeof credentials !== 'object') {
                        throw Boom.badImplementation('Bad credentials object received for Firebase ID auth validation');
                    }                    
                    return h.authenticated({ credentials });
                }
            };
        });
        server.auth.strategy('firebase', 'firebase-auth');
    }
};

export const plugin = {
    name: 'firebase-auth',
    version: '1.0.0',
    register: internals.implementation,
};