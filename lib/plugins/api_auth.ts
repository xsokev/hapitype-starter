'use strict';

import { Request, ResponseToolkit, Server } from "@hapi/hapi";

const Boom = require('@hapi/boom');
const Hoek = require('@hapi/hoek');

const internals = {
    implementation: function (server: Server, options: any) {
        Hoek.assert(options, 'Missing API Key auth strategy options');
        Hoek.assert((typeof options.apiKey === 'string' || typeof options.api_key === 'string'), 'options.apiKey must be a valid string in api-key scheme');

        const apiKey = options.apiKey || options.api_key;

        server.auth.scheme('api-key-auth', (server) => {
            return {
                authenticate: async function (request: Request, h: ResponseToolkit) {
                    const headerApiKey = request.raw.req.headers.api_key;
                    if (!headerApiKey) {
                        throw Boom.unauthorized(null, 'API Key', options.unauthorizedAttributes);
                    }
                    const isValid = headerApiKey === apiKey;
                    console.log(headerApiKey, isValid);
    
                    if (!isValid) {
                        return h.unauthenticated(Boom.unauthorized('Invalid API Key', 'API Key', options.unauthorizedAttributes));
                    }
                    return h.authenticated({credentials: apiKey});
                }
            };
        });
        server.auth.strategy('api-key', 'api-key-auth');
    }
};

export const plugin = {
    name: 'api-key-auth',
    version: '1.0.0',
    register: internals.implementation,
};