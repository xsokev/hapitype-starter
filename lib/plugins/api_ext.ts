'use strict';

import { Request, ResponseToolkit, Server } from "@hapi/hapi";

const Boom = require('@hapi/boom');
const Hoek = require('@hapi/hoek');

function apiExt(server: Server, options: any) {
    Hoek.assert(options, 'Missing API Key extension options');
    Hoek.assert((typeof options.apiKey === 'string' || typeof options.api_key === 'string'), 'options.apiKey must be a valid string in api-key extension');

    const apiKey = options.apiKey || options.api_key;

    server.ext('onRequest', function(request: Request, h: ResponseToolkit){
        const headerApiKey = request.raw.req.headers.api_key;
        if (!headerApiKey) {
            throw Boom.unauthorized(null, 'API Key');
        }
        const isValid = headerApiKey === options.apiKey;

        if (!isValid) {
            throw Boom.unauthorized('Invalid API Key', 'API Key');
        }
        return h.continue;
    })    
}

export const plugin = {
    name: 'api-key-ext',
    version: '1.0.0',
    register: apiExt,
};