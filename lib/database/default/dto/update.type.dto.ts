import Joi from "joi";

export const UpdateTypeSchema = Joi.object({
  name: Joi.string().alphanum().optional(),
  label: Joi.string().alphanum().optional(),
  parent: Joi.number().integer().optional(),
})
