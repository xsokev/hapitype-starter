import Joi from "joi";

export const CreateTypeSchema = Joi.object({
  name: Joi.string().alphanum().required(),
  label: Joi.string().alphanum().required(),
  parent: Joi.number().integer().optional(),
})
