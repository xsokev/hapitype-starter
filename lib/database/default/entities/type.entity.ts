import { BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import {TimestampEntity} from './base.entity'

@Entity()
export class Type extends TimestampEntity {
  @Column()
  name: string;

  @Column()
  label: string;

  @Column({ nullable: true })
  parent?: number;
}
